# Magenda 
[![Build status](https://gitlab.com/juliendehos/magenda/badges/master/build.svg)](https://gitlab.com/juliendehos/magenda/pipelines) 

C++ GUI for iCalendar (calendar and todo)

![](image_magenda.jpg)

## Build & install
install libical gtkmm-3.0 
```
make
sudo make install
magenda
```

## TODO
- multiple calendars
- network calendars
- timezones

## License
[WTFPL](http://www.wtfpl.net)

