# Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

PACKAGES = gtkmm-3.0 libical fontconfig
override CXXFLAGS += -std=c++11 -Wall -Wextra
MAINSRC = ./src/magenda.cpp
ifeq ($(DEBUG), 1)
	override CXXFLAGS += -O0 -g
else
	override CXXFLAGS += -O1 -DNDEBUG
endif

override CXXFLAGS += `pkg-config --cflags $(PACKAGES)`
override LDFLAGS +=`pkg-config --libs-only-L --libs-only-other $(PACKAGES)`
override LIBS +=`pkg-config --libs-only-l $(PACKAGES)`
BINDIR = ./bin
OBJDIR = ./obj
SRCDIR = ./src
SRC = $(filter-out $(MAINSRC), $(shell find $(SRCDIR) -name "*.cpp"))
OBJ = $(subst $(SRCDIR)/, $(OBJDIR)/, $(SRC:.cpp=.o))
BIN = $(subst $(SRCDIR)/, $(BINDIR)/, $(MAINSRC:.cpp=.out))

.PHONY : all clean install uninstall
.SECONDARY:

ALL: $(BIN)
	@echo "Use \"make DEBUG=1\" for developping the code !!!"
$(BINDIR)/%.out: $(OBJ) $(OBJDIR)/%.o
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -c $< -o $@
clean:
	rm -f $(BIN)
	find $(OBJDIR) -name "*.o" | xargs rm
install: $(BIN) 
	cp ./bin/magenda.out $(prefix)/bin/magenda
uninstall:
	rm /usr/local/bin/magenda

