with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "magenda";

  buildInputs = [ stdenv gtkmm3 libical xorg.libxcb fontconfig makeWrapper ];

  nativeBuildInputs = [ cmake pkgconfig ];

  enableParallelBuilding = true;

  src = ./.;

  preFixup = ''
    wrapProgram "$out/bin/magenda" \
      --prefix XDG_DATA_DIRS : "$GSETTINGS_SCHEMAS_PATH"
  '';

}

