// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewTodo.hpp"
#include "ViewTodoDialog.hpp"
#include "View.hpp"

ViewTodo::ViewTodo(View & refView) :
_refView(refView) {

    ViewTodoButton * ptrButton = Gtk::manage(new ViewTodoButton(refView, *this, -1, "New todo..."));
    
    Gtk::ScrolledWindow * ptrScrolledWindow = Gtk::manage(new Gtk::ScrolledWindow);
    ptrScrolledWindow->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    ptrScrolledWindow->add(_todoVBox);

    pack_start(*ptrButton, Gtk::PACK_SHRINK);
    pack_start(*ptrScrolledWindow);
}


void ViewTodo::runTodoDialog(int todoIndex) {

    bool isNewTodo = false;
    std::string title = "Update existing todo...";

    if (todoIndex == -1) {
        isNewTodo = true;
        title = "Add new todo...";
        todoIndex = _refView.newTodo();
    }

    Todo & todo = _refView.getTodo(todoIndex);

    ViewTodoDialog dialog(todo, title);
    int ret = dialog.run();

    switch (ret) {
        case ViewTodoDialog::VIEWTODODIALOG_UPDATE:
            dialog.updateTodo(todo);
            _refView.setUnsaved();
            break;
        case ViewTodoDialog::VIEWTODODIALOG_REMOVE:  
            _refView.deleteTodo(todoIndex);
            _refView.setUnsaved();
            break;
        default: 
            if (isNewTodo) _refView.deleteTodo(todoIndex);
            break;
    }

    refresh();
}

void ViewTodo::refresh() {

    _todoVBox.hide();
    _todoButtons.clear();

    _todoButtons.reserve(_refView.nbTodos());
    for (int i=0; i<_refView.nbTodos(); i++) {
        Todo & t = _refView.getTodo(i);
        _todoButtons.emplace_back(_refView, *this, i, t);
    }

    for (ViewTodoButton & button : _todoButtons) {
        _todoVBox.pack_start(button, Gtk::PACK_SHRINK);
    }
    _todoVBox.show_all();
}

