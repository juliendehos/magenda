// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewTodoDialog.hpp"

#include "View.hpp"

ViewTodoDialog::ViewTodoDialog(const Todo & todo, const std::string & title) {

    Gtk::VBox * ptrVBox = (Gtk::VBox*)get_vbox();

    // summary
    Gtk::Label * ptrSummaryLabel = Gtk::manage(new Gtk::Label("\nSummary:"));
    ptrSummaryLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrSummaryLabel, Gtk::PACK_SHRINK);
    _summaryEntry.set_text(todo._summary);
    ptrVBox->pack_start(_summaryEntry, Gtk::PACK_SHRINK);

    // location
    Gtk::Label * ptrLocationLabel = Gtk::manage(new Gtk::Label("\nLocation:"));
    ptrLocationLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrLocationLabel, Gtk::PACK_SHRINK);
    _locationEntry.set_text(todo._location);
    ptrVBox->pack_start(_locationEntry, Gtk::PACK_SHRINK);

    // description
    Gtk::Label * ptrDescriptionLabel = new Gtk::Label("Description:");
    ptrDescriptionLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrDescriptionLabel, Gtk::PACK_SHRINK);
    _descriptionTextView.get_buffer()->set_text(todo._description);
    ptrVBox->pack_start(_descriptionTextView);
    
    add_button("Remove", VIEWTODODIALOG_REMOVE);
    add_button("Cancel", VIEWTODODIALOG_CANCEL);
    add_button("Update", VIEWTODODIALOG_UPDATE);

    set_title(title);
    resize(300, 300);
    show_all();
}


void ViewTodoDialog::updateTodo(Todo & refTodo) const {
    refTodo._summary = _summaryEntry.get_buffer()->get_text();
    refTodo._location = _locationEntry.get_buffer()->get_text();
    refTodo._description = _descriptionTextView.get_buffer()->get_text();
}

