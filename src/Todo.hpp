// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef TODO_HPP_
#define TODO_HPP_

#include <libical/ical.h>
#include <libical/icalss.h>

#include <string>

// TODO refactor ? (class Event)
class Todo {

    private:
        icalcomponent * _ptrIcal;

    public:
        std::string _summary;
        std::string _location;
        std::string _description;

    public:
        Todo(const std::string & summary, const std::string & location, const std::string & description);
        Todo();
        Todo(icalcomponent * ptrIcal);
        Todo (const Todo & event);

        icalcomponent * getPtrIcal() const;

        void updateIcalFromData();
        void updateDataFromIcal();

    private:
        void convertIcalToString(std::string & refString, icalproperty * ptrProperty);
        void removeProperty(icalproperty_kind kind);
};

#endif

