// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWTODO_HPP_
#define VIEWTODO_HPP_

#include "ViewTodoButton.hpp"

class View;

class ViewTodo : public Gtk::VBox {

    View & _refView;
    Gtk::VBox _todoVBox;
    std::vector<ViewTodoButton> _todoButtons;

public:

    ViewTodo(View & refView);
    void runTodoDialog(int todoIndex);
    void refresh();

};

#endif

