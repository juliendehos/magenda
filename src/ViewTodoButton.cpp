// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewTodoButton.hpp"
#include "View.hpp"
#include "ViewTodo.hpp"

ViewTodoButton::ViewTodoButton(View & refView, ViewTodo & refViewTodo, int todoIndex, const Glib::ustring & text) :
_refView(refView), _refViewTodo(refViewTodo), _todoIndex(todoIndex), _label(text) { 

    add(_label);
    set_alignment(0, 0);
    _label.set_use_markup();
}

ViewTodoButton::ViewTodoButton(View & refView, ViewTodo & refViewTodo, int todoIndex, const Todo & refTodo) : 
ViewTodoButton(refView, refViewTodo, todoIndex, "") { 

    std::stringstream ss;
    ss << "<b>" << refTodo._summary << "</b>\n"; 

    if (not refTodo._location.empty()) ss << "\n" << refTodo._location << "\n";
    if (not refTodo._description.empty()) ss << "\n" << refTodo._description << "\n";

    _label.set_text(ss.str());
    _label.set_use_markup();
}

ViewTodoButton::ViewTodoButton(const ViewTodoButton & b) : ViewTodoButton(b._refView, b._refViewTodo, b._todoIndex, b.get_label()) {
}

void ViewTodoButton::on_clicked() {
    _refViewTodo.runTodoDialog(_todoIndex);
}

