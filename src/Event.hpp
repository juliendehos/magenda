// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef EVENT_HPP_
#define EVENT_HPP_

#include <libical/ical.h>
#include <libical/icalss.h>

#include <string>
#include <ctime>

class Event {

    // TODO handle timezone

    private:
        icalcomponent * _ptrIcal;

    public:
        std::time_t _startTime;
        std::time_t _endTime;
        std::string _summary;
        std::string _location;
        std::string _description;

    public:
        Event(const std::time_t & startTime, const std::time_t & endTime, 
                const std::string & summary, const std::string & location, 
                const std::string & description);
        Event();
        Event(icalcomponent * ptrIcal);
        Event (const Event & event);

        icalcomponent * getPtrIcal() const;

        void updateIcalFromData();
        void updateDataFromIcal();

        static std::time_t convertLocaltoUtc(std::time_t localTime);
        static std::time_t convertUtcToLocal(std::time_t utcTime);

    private:
        void convertIcalToString(std::string & refString, icalproperty * ptrProperty);
        void convertIcalToStartTime(std::time_t & refTime, icalproperty * ptrProperty);
        void convertIcalToEndTime(std::time_t & refTime, icalproperty * ptrProperty);
        void removeProperty(icalproperty_kind kind);
};

#endif

