// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "View.hpp"

#include <cassert>
#include <fstream>
#include <gtkmm.h>
#include <iostream>

//TODO colorized agenda (for multiple agenda)
//TODO sort events of day

View::View(int & argc, char** argv) : 
Gtk::Main(argc, argv), _unsavedAgenda(false), _menu(*this), _viewEvent(*this), _viewTodo(*this) {

    Gtk::Notebook * ptrNotebook = Gtk::manage(new Gtk::Notebook);
    ptrNotebook->append_page(_viewEvent, "Events");
    ptrNotebook->append_page(_viewTodo, "Todos");

    _vbox.pack_start(_menu.init(_window), Gtk::PACK_SHRINK);
    _vbox.pack_start(*ptrNotebook);
    _vbox.pack_start(_statusbar, Gtk::PACK_SHRINK);

    _window.add(_vbox);
    _window.set_size_request(400, 600);
    _window.show_all();

    if (argc > 1)
        _agenda.openFile(argv[1]);

    refresh();
}

void View::run() {
    Gtk::Main::run(_window);
}

void View::refresh() {
    displayStatus(_agenda.getAgendaFilename());
    _viewEvent.refresh();
    _viewTodo.refresh();
}

void View::quit() {
    if(checkUnsavedAgenda()) 
        Gtk::Main::quit();
}

void View::newFile() {
    if(checkUnsavedAgenda()) {
        _agenda.reset();
        refresh();

    }
}

void View::openFile() {
    if(checkUnsavedAgenda()) { 
        Gtk::FileChooserDialog dialog("Open file...");
        Glib::RefPtr<Gtk::FileFilter> filter = Gtk::FileFilter::create();
        filter->set_name("ical files (*.ics)");
        filter->add_pattern("*.ics");
        dialog.add_filter(filter);
        dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
        dialog.add_button("_Open", Gtk::RESPONSE_OK);
        dialog.set_transient_for(_window);
        int result = dialog.run();
        if (result == Gtk::RESPONSE_OK) {
            _agenda.openFile(dialog.get_filename());
            refresh();
        }
    }
}

bool View::canSaveFile() const {
    return _agenda.canSaveFile();
}

void View::saveFile() {
    if (canSaveFile()) {
        _agenda.saveFile();
        _unsavedAgenda = false;
    }
    else {
        saveAsFile();
    }
}

void View::saveAsFile() {
    Gtk::FileChooserDialog dialog("Save as...", Gtk::FILE_CHOOSER_ACTION_SAVE);
    Glib::RefPtr<Gtk::FileFilter> filter = Gtk::FileFilter::create();
    filter->set_name("ical files (*.ics)");
    filter->add_pattern("*.ics");
    dialog.add_filter(filter);
    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("_Save", Gtk::RESPONSE_OK);
    dialog.set_transient_for(_window);
    int result = dialog.run();
    if (result == Gtk::RESPONSE_OK) {
        std::string filename = dialog.get_filename();
        if (filename.substr(filename.size()-4, 4) != ".ics")
            filename += ".ics";
        _agenda.saveAsFile(filename);
        _unsavedAgenda = false;
        refresh();
    }
}


void View::displayStatus(const std::string & refMessage) {
    _statusbar.pop();
    _statusbar.push(refMessage);
}

void View::displayMessageDialog(const std::string & refTitle, const std::string & refMessage) {
    Glib::ustring title(refTitle.c_str());
    Glib::ustring message(refMessage.c_str());
    Gtk::MessageDialog dialog(_window, message);
    dialog.set_title(title);
    dialog.run();
}

bool View::displayConfirmDialog(const std::string & refTitle, const std::string & refMessage) {
    Glib::ustring title(refTitle.c_str());
    Glib::ustring message(refMessage.c_str());
    Gtk::MessageDialog dialog(_window, message, false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO);
    dialog.set_title(title);
    int ret = dialog.run();
    return ret == Gtk::RESPONSE_YES;
}

int View::convertTimetToDay(const std::time_t & time) {
    std::tm * tm = std::gmtime(&time);
    return tm->tm_mday;
}

std::time_t View::convertDateToTimet(int year, int month, int day, int hour, int minute, int second) {
    std::tm t = {second, minute, hour, day, month-1, year-1900, 0, 0, 0, 0, 0};
    return timegm(&t);
}

void View::convertTimetToDate(std::time_t time, int& year, int& month, int& day, int& hour, int& minute, int& second) {
    std::tm * tm = std::gmtime(&time);
    year = tm->tm_year+1900;
    month = tm->tm_mon+1;
    day = tm->tm_mday;
    hour = tm->tm_hour;
    minute = tm->tm_min;
    second = tm->tm_sec;
}

bool View::checkUnsavedAgenda() {
    if (_unsavedAgenda) {
        Glib::ustring title = "Unsaved data detected...";
        Glib::ustring message = "Save data ?";
        Gtk::MessageDialog dialog(_window, message, false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_NONE);
        dialog.add_button("Yes", Gtk::RESPONSE_YES);
        dialog.add_button("No", Gtk::RESPONSE_NO);
        dialog.add_button("Cancel", Gtk::RESPONSE_CANCEL);
        dialog.set_title(title);
        int ret = dialog.run();
        if (ret == Gtk::RESPONSE_CANCEL)
            return false;
        if (ret == Gtk::RESPONSE_YES)
            saveFile();
        _unsavedAgenda = false;
        return true;
    }

    return true;
}

void View::setUnsaved() {
    _unsavedAgenda = true;
}

std::vector<int> View::computeEvents(const std::time_t & fromTime, const std::time_t & toTime) const {
    return _agenda.computeEvents(fromTime, toTime);
}

int View::newEvent(unsigned year, unsigned month, unsigned day) {
    std::time_t startTime = convertDateToTimet(year, month, day, 9, 0, 0);
    std::time_t endTime = convertDateToTimet(year, month, day, 12, 0, 0);
    Event e(startTime, endTime, "", "", "");
    _agenda._events.push_back(e);
    return _agenda._events.size() - 1;
}

void View::deleteEvent(int index) {
    assert(index >= 0);
    assert(index < _agenda._events.size());
    int last = _agenda._events.size() - 1;
    if (index != last)
        std::swap(_agenda._events[index], _agenda._events[last]);
    _agenda._events.pop_back();
}

Event & View::getEvent(int index) {
    assert(index >= 0);
    assert(index < _agenda._events.size());
    return _agenda._events[index];
}

int View::nbEvents() const {
    return _agenda._events.size();
}

int View::newTodo() {
    Todo t("", "", "");
    _agenda._todos.push_back(t);
    return _agenda._todos.size() - 1;
}

void View::deleteTodo(int index) {
    assert(index >= 0);
    assert(index < _agenda._todos.size());
    int last = _agenda._todos.size() - 1;
    if (index != last)
        std::swap(_agenda._todos[index], _agenda._todos[last]);
    _agenda._todos.pop_back();
}

Todo & View::getTodo(int index) {
    assert(index >= 0);
    assert(index < _agenda._todos.size());
    return _agenda._todos[index];
}

int View::nbTodos() const {
    return _agenda._todos.size();
}


