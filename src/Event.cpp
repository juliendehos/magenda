// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Event.hpp"

#include <iostream>

Event::Event(const std::time_t & startTime, const std::time_t & endTime, const std::string & summary, const std::string & location, const std::string & description) :
    _ptrIcal(0), _startTime(startTime), _endTime(endTime), _summary(summary), _location(location), _description(description) {
        updateIcalFromData();
    }

Event::Event() : _ptrIcal(nullptr) {
}

Event::Event(icalcomponent * ptrIcal) {
    _ptrIcal = icalcomponent_new_clone(ptrIcal);
    updateDataFromIcal();
}

Event::Event (const Event & event) : 
_startTime(event._startTime), _endTime(event._endTime),
_summary(event._summary), _location(event._location), _description(event._description) {

    if (event._ptrIcal) {
        _ptrIcal = icalcomponent_new_clone(event._ptrIcal);
    }
    else {
        _ptrIcal = nullptr;
    }
}

icalcomponent * Event::getPtrIcal() const {
    return _ptrIcal;
}

void Event::updateIcalFromData() {

    // clean event
    if (_ptrIcal) {
        assert(icalcomponent_isa(_ptrIcal) == ICAL_VEVENT_COMPONENT);

        removeProperty(ICAL_SUMMARY_PROPERTY);
        removeProperty(ICAL_LOCATION_PROPERTY);
        removeProperty(ICAL_DESCRIPTION_PROPERTY);
        removeProperty(ICAL_DTSTART_PROPERTY);
        removeProperty(ICAL_DTEND_PROPERTY);
    }
    else {
        _ptrIcal = icalcomponent_new(ICAL_VEVENT_COMPONENT);
        assert(_ptrIcal);
    }

    // add new properties
    if (not _summary.empty())
        icalcomponent_add_property(_ptrIcal, icalproperty_new_summary(_summary.c_str()));

    if (not _location.empty())
        icalcomponent_add_property(_ptrIcal, icalproperty_new_location(_location.c_str()));

    if (not _description.empty())
        icalcomponent_add_property(_ptrIcal, icalproperty_new_description(_description.c_str()));

    icaltimetype startTime = icaltime_from_timet(_startTime, 0);
    icalcomponent_add_property(_ptrIcal, icalproperty_new_dtstart(startTime));

    icaltimetype endTime = icaltime_from_timet(_endTime, 0);
    icalcomponent_add_property(_ptrIcal, icalproperty_new_dtend(endTime));
}

void Event::updateDataFromIcal() {
    assert(_ptrIcal);
    assert(icalcomponent_isa(_ptrIcal) == ICAL_VEVENT_COMPONENT);

    for (icalproperty * ptrProperty = icalcomponent_get_first_property(_ptrIcal, ICAL_ANY_PROPERTY);
            ptrProperty != 0;
            ptrProperty = icalcomponent_get_next_property(_ptrIcal, ICAL_ANY_PROPERTY)) {

        icalproperty_kind propertyKind = icalproperty_isa(ptrProperty);

        switch (propertyKind) {
            case ICAL_SUMMARY_PROPERTY:     convertIcalToString(_summary, ptrProperty);         break;
            case ICAL_LOCATION_PROPERTY:    convertIcalToString(_location, ptrProperty);        break;
            case ICAL_DESCRIPTION_PROPERTY: convertIcalToString(_description, ptrProperty);     break;
            case ICAL_DTSTART_PROPERTY:     convertIcalToStartTime(_startTime, ptrProperty);    break;
            case ICAL_DTEND_PROPERTY:       convertIcalToEndTime(_endTime, ptrProperty);        break;
            default: break;
        }
    }
}

void Event::convertIcalToString(std::string & refString, icalproperty * ptrProperty) {
    refString = std::string(icalproperty_get_value_as_string(ptrProperty));
    // TODO check if libical has a function to remove escaped characters
    while (true) {
        std::string::size_type pos = refString.find("\\n");
        if (pos == std::string::npos) break;
        refString.replace(pos, 2, "\n");
    }
    while (true) {
        std::string::size_type pos = refString.find("\\t");
        if (pos == std::string::npos) break;
        refString.replace(pos, 2, "\t");
    }
    while (true) {
        std::string::size_type pos = refString.find("\\");
        if (pos == std::string::npos) break;
        refString.replace(pos, 1, "");
    }
}

void Event::convertIcalToStartTime(std::time_t & refTime, icalproperty * ptrProperty) {
    icaltimetype icalTime = icalproperty_get_dtstart(ptrProperty);
    refTime = std::time_t(icaltime_as_timet(icalTime));
    // TODO handle timezone 
}

void Event::convertIcalToEndTime(std::time_t & refTime, icalproperty * ptrProperty) {
    icaltimetype icalTime = icalproperty_get_dtend(ptrProperty);
    refTime = std::time_t(icaltime_as_timet(icalTime));
    // TODO handle timezone 
}


void Event::removeProperty(icalproperty_kind kind) {

    while (true) {

        icalproperty * ptrOldProperty = icalcomponent_get_first_property(_ptrIcal, kind);

        if (ptrOldProperty == nullptr) 
            break;
        else
            icalcomponent_remove_property(_ptrIcal, ptrOldProperty);
    }
}


/*
   std::time_t Event::convertLocaltoUtc(std::time_t localTime) {
   std::tm * tm = std::gmtime(&localTime);
   tm->tm_isdst = -1;
   return mktime(tm);
   }

   std::time_t Event::convertUtcToLocal(std::time_t utcTime) {
   std::tm * tm = std::localtime(&utcTime);
   tm->tm_isdst =1;
   return timegm(tm);
   }

   void Event::convertIcalToStartTime(std::time_t & refTime, icalproperty * ptrProperty) {
   icaltimetype icalTime = icalproperty_get_dtstart(ptrProperty);
   std::time_t t = std::time_t(icaltime_as_timet(icalTime));
   refTime = convertLocaltoUtc(t);
}

void Event::convertIcalToEndTime(std::time_t & refTime, icalproperty * ptrProperty) {
icaltimetype icalTime = icalproperty_get_dtend(ptrProperty);
std::time_t t = std::time_t(icaltime_as_timet(icalTime));
refTime = convertLocaltoUtc(t);
}

void Event::convertIcalToEndTime(std::time_t & refTime, icalproperty * ptrProperty) {

icaltimetype icalTime = icalproperty_get_dtend(ptrProperty);

if (not icaltime_is_utc(icalTime)) {
icaltimezone * ptrTimezone = (icaltimezone*)icaltime_get_timezone(icalTime);
icaltimezone * ptrTimezoneUtc = icaltimezone_get_utc_timezone();
icaltimezone_convert_time(&icalTime, ptrTimezone, ptrTimezoneUtc);
}

refTime = std::time_t(icaltime_as_timet(icalTime));

if (icaltime_is_date(icalTime)) {
refTime += 3600*12;
std::cerr << "warning: day event not implemented\n";
}
}
*/

