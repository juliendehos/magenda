// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewMenu.hpp"

#include "View.hpp"

ViewMenu::ViewMenu(View & refView) :
    _refView(refView) {
    }

Gtk::Widget & ViewMenu::init(Gtk::Window & refWindow) {

    _ptrActionGroup = Gtk::ActionGroup::create();

    // menu jeu
    _ptrActionGroup->add(Gtk::Action::create("MenuFile", "_File"));
    _ptrActionGroup->add(Gtk::Action::create("MenuFileNew", "_New"), Gtk::AccelKey("<control>n"), sigc::mem_fun(_refView, &View::newFile));
    _ptrActionGroup->add(Gtk::Action::create("MenuFileOpen", "_Open..."), Gtk::AccelKey("<control>o"), sigc::mem_fun(_refView, &View::openFile));
    _ptrActionGroup->add(Gtk::Action::create("MenuFileSave", "_Save"), Gtk::AccelKey("<control>s"), sigc::mem_fun(_refView, &View::saveFile));
    _ptrActionGroup->add(Gtk::Action::create("MenuFileSaveAs", "_Save as..."), Gtk::AccelKey("<control><shift>s"), sigc::mem_fun(_refView, &View::saveAsFile));
    _ptrActionGroup->add(Gtk::Action::create("MenuFileQuit", "_Quit"), Gtk::AccelKey("<control>q"), sigc::mem_fun(_refView, &View::quit));

    // menu aide
    _ptrActionGroup->add(Gtk::Action::create("MenuHelp", "_Help"));
    _ptrActionGroup->add(Gtk::Action::create("MenuHelpAbout", "_About..."), sigc::mem_fun(*this, &ViewMenu::handleMenuAbout));

    _ptrUIManager = Gtk::UIManager::create();
    _ptrUIManager->insert_action_group(_ptrActionGroup);
    refWindow.add_accel_group(_ptrUIManager->get_accel_group());

    Glib::ustring uiInfo;
    uiInfo += "<ui>";
    uiInfo += "  <menubar name='MenuBar'>";
    uiInfo += "    <menu action='MenuFile'>";
    uiInfo += "      <menuitem action='MenuFileNew'/>";
    uiInfo += "      <menuitem action='MenuFileOpen'/>";
    uiInfo += "      <menuitem action='MenuFileSave'/>";
    uiInfo += "      <menuitem action='MenuFileSaveAs'/>";
    uiInfo += "      <menuitem action='MenuFileQuit'/>";
    uiInfo += "    </menu>";
    uiInfo += "    <menu action='MenuHelp'>";
    uiInfo += "      <menuitem action='MenuHelpAbout'/>";
    uiInfo += "    </menu>";
    uiInfo += "  </menubar>";
    uiInfo += "</ui>";

    _ptrUIManager->add_ui_from_string(uiInfo);

    return *(_ptrUIManager->get_widget("/MenuBar"));
}

void ViewMenu::handleMenuAbout() {
    Gtk::AboutDialog dialog;
    dialog.set_program_name("magenda");
    dialog.set_version("1.0");
    dialog.set_website("http://www-lisic.univ-littoral.fr/~dehos");
    dialog.run();
}
