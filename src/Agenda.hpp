// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef AGENDA_HPP_
#define AGENDA_HPP_

#include "Event.hpp"
#include "Todo.hpp"

#include <string>
#include <vector>

class Agenda {

    // TODO handle remote calendar

    const std::string _configFilename = ".magenda";
    std::string _configPath;
    std::string _agendaFilename;

// TODO change to private
public:
    std::vector<Event> _events;
    std::vector<Todo> _todos;

    public:
    Agenda();
    void reset();

    const std::string & getAgendaFilename() const;

    void openFile(const std::string & filename);
    void saveFile();
    bool canSaveFile() const;
    void saveAsFile(const std::string & filename);
    static char * getLine(char * str, size_t size, void * fd);

    // events
    std::vector<int> computeEvents(const std::time_t & fromTime, const std::time_t & toTime) const;

    // todos

    private:
    void setAgendaFilename(const std::string & filename);
};

#endif

