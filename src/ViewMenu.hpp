// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWMENU_HPP_
#define VIEWMENU_HPP_

#include <gtkmm.h>

class View;

class ViewMenu {

	View & _refView;

	Glib::RefPtr<Gtk::UIManager> _ptrUIManager;
	Glib::RefPtr<Gtk::ActionGroup> _ptrActionGroup;

public:
	ViewMenu(View&);
	Gtk::Widget & init(Gtk::Window & refWindow);

private:
	void handleMenuAbout();
};

#endif
