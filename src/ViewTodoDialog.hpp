// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWTODODIALOG_HPP_
#define VIEWTODODIALOG_HPP_

#include <gtkmm.h>

#include "Todo.hpp"

class ViewTodoDialog : public Gtk::Dialog {

    public:
    enum response_t {VIEWTODODIALOG_CANCEL, VIEWTODODIALOG_UPDATE, VIEWTODODIALOG_REMOVE};

    public:
    ViewTodoDialog(const Todo & refTodo, const std::string & title);
    void updateTodo(Todo & refTodo) const;

    private:
    Gtk::Entry _summaryEntry;
    Gtk::Entry _locationEntry;
    Gtk::TextView _descriptionTextView;
};

#endif

