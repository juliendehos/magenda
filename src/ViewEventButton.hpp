// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWEVENTBUTTON_HPP_
#define VIEWEVENTBUTTON_HPP_

#include <gtkmm.h>

#include "Event.hpp"

class View;
class ViewEvent;

class ViewEventButton : public Gtk::Button {
    View & _refView;
    ViewEvent & _refViewEvent;
    int _eventIndex;
    Gtk::Label _label;

    public:
    ViewEventButton(View & refView, ViewEvent & refViewEvent, int eventIndex, const Glib::ustring & text);
    ViewEventButton(View & refView, ViewEvent & refViewEvent, int eventIndex, const Event & refEvent);
	ViewEventButton(const ViewEventButton & b);

    protected:
    void on_clicked();
};

#endif

