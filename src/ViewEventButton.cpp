// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewEventButton.hpp"
#include "View.hpp"
#include "ViewEvent.hpp"

ViewEventButton::ViewEventButton(View & refView, ViewEvent & refViewEvent, int eventIndex, const Glib::ustring & text) :
 _refView(refView), _refViewEvent(refViewEvent), _eventIndex(eventIndex), _label(text) { 

    add(_label);
    set_alignment(0, 0);
    _label.set_use_markup();
}

ViewEventButton::ViewEventButton(View & refView, ViewEvent & refViewEvent, int eventIndex, const Event & refEvent) : 
ViewEventButton(refView, refViewEvent, eventIndex, "") { 

    std::stringstream ss;
    ss << "<b>" << refEvent._summary << "</b>\n\n"; 

    ss << std::asctime(std::gmtime(&refEvent._startTime));
    ss << std::asctime(std::gmtime(&refEvent._endTime));
    // TODO timezone
    //ss << std::ctime(&refEvent._startTime);
    //ss << std::ctime(&refEvent._endTime);
    if (not refEvent._location.empty()) ss << "\n" << refEvent._location << "\n";
    if (not refEvent._description.empty()) ss << "\n" << refEvent._description << "\n";

    _label.set_text(ss.str());
    _label.set_use_markup();
}

ViewEventButton::ViewEventButton(const ViewEventButton & b) : ViewEventButton(b._refView, b._refViewEvent, b._eventIndex, b._label.get_text()) {
}

void ViewEventButton::on_clicked() {
    _refViewEvent.runEventDialog(_eventIndex);
}

