// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewEvent.hpp"
#include "View.hpp"
#include "ViewEventDialog.hpp"
#include <algorithm>

ViewEvent::ViewEvent(View & refView) :
_refView(refView) {

    Gtk::ScrolledWindow * ptrEventsScrolledWindow = Gtk::manage(new Gtk::ScrolledWindow);
    ptrEventsScrolledWindow->set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    ptrEventsScrolledWindow->add(_eventVBox);

    _calendar.set_display_options( Gtk::CALENDAR_SHOW_HEADING | 
                    Gtk::CALENDAR_SHOW_DAY_NAMES |
                    Gtk::CALENDAR_SHOW_WEEK_NUMBERS ); 

    pack_start(_calendar, Gtk::PACK_SHRINK);
    pack_start(*ptrEventsScrolledWindow);

    _calendar.signal_month_changed().connect(sigc::mem_fun(_refView, &View::refresh));
    _calendar.signal_day_selected().connect(sigc::mem_fun(_refView, &View::refresh));
    _calendar.signal_day_selected_double_click().connect(sigc::mem_fun(*this, &ViewEvent::handleDoubleClickCalendar));

}

void ViewEvent::getSelectedDate(unsigned & year, unsigned & month, unsigned & day) const {
    _calendar.get_date(year, month, day);
    month++;    // get_date gives month in [0, 11]
}

void ViewEvent::handleDoubleClickCalendar() {
    unsigned year, month, day;
    getSelectedDate(year, month, day);
    runEventDialog(-1);
}

void ViewEvent::refresh() {
    // TODO sort event list by start time

    // clear previous data
    _calendar.clear_marks();
    _eventVBox.hide();
    _eventButtons.clear();

    // get selected date
    unsigned year, month, day;
    getSelectedDate(year, month, day);

    // update calendar
    std::time_t monthStartTime = View::convertDateToTimet(year, month, 1, 0, 0, 0);
    std::time_t monthEndTime = View::convertDateToTimet(year, month, 31, 23, 59, 59);
    std::vector<int> monthEventIndices = _refView.computeEvents(monthStartTime, monthEndTime);
    for (int i : monthEventIndices) {
        const Event & event = _refView.getEvent(i);
        int startDay = View::convertTimetToDay(std::max(monthStartTime, event._startTime));
        int endDay = View::convertTimetToDay(std::min(monthEndTime, event._endTime));
        for (int day=startDay; day<=endDay; day++)
            _calendar.mark_day(day);
    }

    // update event list
    std::time_t dayStartTime = View::convertDateToTimet(year, month, day, 0, 0, 0);
    std::time_t dayEndTime = View::convertDateToTimet(year, month, day, 23, 59, 59);
    std::vector<int> dayEventIndices = _refView.computeEvents(dayStartTime, dayEndTime);
    auto fCompareEvents = [this] (int i1, int i2) {
        const Event e1 = this->_refView.getEvent(i1);
        const Event e2 = this->_refView.getEvent(i2);
        return e1._startTime <= e2._startTime;
    };
    std::sort(dayEventIndices.begin(), dayEventIndices.end(), fCompareEvents);

    _eventButtons.reserve(dayEventIndices.size());
    for (int index : dayEventIndices) { 
        const Event & event = _refView.getEvent(index);
        _eventButtons.emplace_back(_refView, *this, index, event);
    }

    for (ViewEventButton & button : _eventButtons) {
        _eventVBox.pack_start(button, Gtk::PACK_SHRINK);
    }
    _eventVBox.show_all();
}

void ViewEvent::runEventDialog(int eventIndex) {

    bool isNewEvent = false;
    std::string title = "Update existing event...";

    if (eventIndex == -1) {
        isNewEvent = true;
        title = "Add new event...";
        unsigned year, month, day;
        getSelectedDate(year, month, day);
        eventIndex = _refView.newEvent(year, month, day);
    }

    Event & event = _refView.getEvent(eventIndex);

    ViewEventDialog dialog(event, title);
    int ret = dialog.run();

    switch (ret) {
        case ViewEventDialog::VIEWEVENTDIALOG_UPDATE:
            dialog.updateEvent(event);
            _refView.setUnsaved();
            break;
        case ViewEventDialog::VIEWEVENTDIALOG_REMOVE:  
            _refView.deleteEvent(eventIndex);
            _refView.setUnsaved();
            break;
        default: 
            if (isNewEvent) _refView.deleteEvent(eventIndex);
            break;
    }

    _refView.refresh();
}


