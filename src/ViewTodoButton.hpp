// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWTODOBUTTON_HPP_
#define VIEWTODOBUTTON_HPP_

#include <gtkmm.h>

#include "Todo.hpp"

class View;
class ViewTodo;

class ViewTodoButton : public Gtk::Button {
    View & _refView;
    ViewTodo & _refViewTodo;
    int _todoIndex;
    Gtk::Label _label;

    public:
    ViewTodoButton(View & refView, ViewTodo & refViewTodo, int todoIndex, const Glib::ustring & text);
    ViewTodoButton(View & refView, ViewTodo & refViewTodo, int todoIndex, const Todo & refTodo);
	ViewTodoButton(const ViewTodoButton & b);

    protected:
    void on_clicked();
};

#endif

