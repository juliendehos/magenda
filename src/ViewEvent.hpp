// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWEVENT_HPP_
#define VIEWEVENT_HPP_

#include "ViewEventButton.hpp"

class View;

class ViewEvent : public Gtk::VBox {

    View & _refView;
    Gtk::Calendar _calendar;
    Gtk::VBox _eventVBox;
    std::vector<ViewEventButton> _eventButtons;

public:

    ViewEvent(View & refView);
    void getSelectedDate(unsigned & year, unsigned & month, unsigned & day) const;
    void runEventDialog(int eventIndex);
    void refresh();

    private:
    void handleDoubleClickCalendar();
};

#endif

