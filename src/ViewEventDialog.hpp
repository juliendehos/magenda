// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEWEVENTDIALOG_HPP_
#define VIEWEVENTDIALOG_HPP_

#include <gtkmm.h>
#include <array>

#include "Event.hpp"

class ViewEventDialog : public Gtk::Dialog {

    public:
    enum response_t {VIEWEVENTDIALOG_CANCEL, VIEWEVENTDIALOG_UPDATE, VIEWEVENTDIALOG_REMOVE};

    public:
    ViewEventDialog(const Event & refEvent, const std::string & title);
    void updateEvent(Event & refEvent) const;

    private:
    typedef std::array<Gtk::SpinButton, 5> spins_t;
    spins_t _startSpins;
    spins_t _endSpins;

    Gtk::Entry _summaryEntry;
    Gtk::Entry _locationEntry;
    Gtk::TextView _descriptionTextView;

    private:
    void configureSpins(spins_t & spins, std::time_t time);
    std::time_t getTimetFromSpinst(const spins_t & spins) const;
};

#endif

