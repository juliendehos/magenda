// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "ViewEventDialog.hpp"

#include "View.hpp"

ViewEventDialog::ViewEventDialog(const Event & event, const std::string & title) {

    Gtk::VBox * ptrVBox = (Gtk::VBox*)get_vbox();

    // start date/time
    Gtk::Label * ptrStartLabel = Gtk::manage(new Gtk::Label("Start:"));
    ptrStartLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrStartLabel, Gtk::PACK_SHRINK);

    Gtk::Label * ptrStartDateLabel = Gtk::manage(new Gtk::Label("    Date: "));
    Gtk::Label * ptrStartTimeLabel = Gtk::manage(new Gtk::Label("    Time: "));

    configureSpins(_startSpins, event._startTime);

    Gtk::HBox * ptrStartHBox = Gtk::manage(new Gtk::HBox());
    ptrStartHBox->pack_start(*ptrStartDateLabel);
    ptrStartHBox->pack_start(_startSpins[0]);
    ptrStartHBox->pack_start(_startSpins[1]);
    ptrStartHBox->pack_start(_startSpins[2]);
    ptrStartHBox->pack_start(*ptrStartTimeLabel);
    ptrStartHBox->pack_start(_startSpins[3]);
    ptrStartHBox->pack_start(_startSpins[4]);
    ptrVBox->pack_start(*ptrStartHBox, Gtk::PACK_SHRINK);

    // end date/time
    Gtk::Label * ptrEndLabel = Gtk::manage(new Gtk::Label("\nEnd:"));
    ptrEndLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrEndLabel, Gtk::PACK_SHRINK);

    Gtk::Label * ptrEndDateLabel = Gtk::manage(new Gtk::Label("    Date: "));
    Gtk::Label * ptrEndTimeLabel = Gtk::manage(new Gtk::Label("    Time: "));

    configureSpins(_endSpins, event._endTime);

    Gtk::HBox * ptrEndHBox = Gtk::manage(new Gtk::HBox());
    ptrEndHBox->pack_start(*ptrEndDateLabel);
    ptrEndHBox->pack_start(_endSpins[0]);
    ptrEndHBox->pack_start(_endSpins[1]);
    ptrEndHBox->pack_start(_endSpins[2]);
    ptrEndHBox->pack_start(*ptrEndTimeLabel);
    ptrEndHBox->pack_start(_endSpins[3]);
    ptrEndHBox->pack_start(_endSpins[4]);
    ptrVBox->pack_start(*ptrEndHBox, Gtk::PACK_SHRINK);

    // summary
    Gtk::Label * ptrSummaryLabel = Gtk::manage(new Gtk::Label("\nSummary:"));
    ptrSummaryLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrSummaryLabel, Gtk::PACK_SHRINK);
    _summaryEntry.set_text(event._summary);
    ptrVBox->pack_start(_summaryEntry, Gtk::PACK_SHRINK);

    // location
    Gtk::Label * ptrLocationLabel = Gtk::manage(new Gtk::Label("\nLocation:"));
    ptrLocationLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrLocationLabel, Gtk::PACK_SHRINK);
    _locationEntry.set_text(event._location);
    ptrVBox->pack_start(_locationEntry, Gtk::PACK_SHRINK);

    // description
    Gtk::Label * ptrDescriptionLabel = new Gtk::Label("\nDescription:");
    ptrDescriptionLabel->set_alignment(0, 0);
    ptrVBox->pack_start(*ptrDescriptionLabel, Gtk::PACK_SHRINK);
    _descriptionTextView.get_buffer()->set_text(event._description);
    ptrVBox->pack_start(_descriptionTextView);
    
    add_button("Remove", VIEWEVENTDIALOG_REMOVE);
    add_button("Cancel", VIEWEVENTDIALOG_CANCEL);
    add_button("Update", VIEWEVENTDIALOG_UPDATE);

    set_title(title);
    resize(300, 500);
    show_all();
}

void ViewEventDialog::configureSpins(spins_t & spins, std::time_t time) {

    int year, month, day, hour, minute, second;
    View::convertTimetToDate(time, year, month, day, hour, minute, second);

    spins[0].set_numeric();
    spins[0].set_range(2000, 2100);
    spins[0].set_increments(1, 1);
    spins[0].set_value(year);

    spins[1].set_numeric();
    spins[1].set_range(1, 12);
    spins[1].set_increments(1, 5);
    spins[1].set_value(month);

    spins[2].set_numeric();
    spins[2].set_range(1, 31);
    spins[2].set_increments(1, 5);
    spins[2].set_value(day);

    spins[3].set_numeric();
    spins[3].set_range(0, 23);
    spins[3].set_increments(1, 5);
    spins[3].set_value(hour);
    
    spins[4].set_numeric();
    spins[4].set_range(0, 59);
    spins[4].set_increments(5, 15);
    spins[4].set_value(minute);
}

std::time_t ViewEventDialog::getTimetFromSpinst(const spins_t & spins) const {
    int year = spins[0].get_value_as_int();
    int month = spins[1].get_value_as_int();
    int day = spins[2].get_value_as_int();
    int hour = spins[3].get_value_as_int();
    int minute = spins[4].get_value_as_int();

    return View::convertDateToTimet(year, month, day, hour, minute, 0);
}

void ViewEventDialog::updateEvent(Event & refEvent) const {
    refEvent._startTime = getTimetFromSpinst(_startSpins);
    refEvent._endTime = getTimetFromSpinst(_endSpins);
    refEvent._summary = _summaryEntry.get_text();
    refEvent._location = _locationEntry.get_text();
    refEvent._description = _descriptionTextView.get_buffer()->get_text();
}

