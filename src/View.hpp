// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#ifndef VIEW_HPP_
#define VIEW_HPP_

#include "Agenda.hpp"
#include "ViewEvent.hpp"
#include "ViewTodo.hpp"
#include "ViewMenu.hpp"

// TODO handle multiple calendar
// TODO handle remote calendar

class View : public Gtk::Main {

    Agenda _agenda;
    bool _unsavedAgenda;

    ViewMenu _menu;
    ViewEvent _viewEvent;
    ViewTodo _viewTodo;
    Gtk::Statusbar _statusbar;

    Gtk::VBox _vbox;
    Gtk::Window _window;

    public :

    // interface
    View(int & argc, char ** argv);
    void run();
    void refresh();
    void quit();

    // file
    void newFile();
    void openFile();
    void saveFile();
    bool canSaveFile() const;
    void saveAsFile();
    bool checkUnsavedAgenda();
    void setUnsaved();

    // events
    std::vector<int> computeEvents(const std::time_t & fromTime, const std::time_t & toTime) const;
    int newEvent(unsigned year, unsigned month, unsigned day);
    void deleteEvent(int index);
    Event & getEvent(int index);
    int nbEvents() const;

    // todos
    int newTodo();
    void deleteTodo(int index);
    Todo & getTodo(int index);
    int nbTodos() const;

    // display dialog box
    void displayStatus(const std::string & refMessage);
    void displayMessageDialog(const std::string & refTitle, const std::string & refMessage);
    bool displayConfirmDialog(const std::string & refTitle, const std::string & refMessage);

    // convert time
    static int convertTimetToDay(const std::time_t & time) ;
    static std::time_t convertDateToTimet(int year, int month, int day, int hour, int minute, int second) ;
    static void convertTimetToDate(std::time_t time, int& year, int& month, int& day, int& hour, int& minute, int& second) ;
};

#endif
