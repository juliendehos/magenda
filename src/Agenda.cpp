// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Agenda.hpp"

#include <unistd.h>
#include <pwd.h>

#include <algorithm>
#include <cstdio>       // fopen, fgets
#include <fstream>
#include <iostream>

Agenda::Agenda() {

    std::string agendaFilename = "";
    passwd * pw = getpwuid(getuid());
    _configPath = std::string(pw->pw_dir) + '/' + _configFilename;

    std::ifstream configFile(_configPath);
    try {
        if (configFile) {
            std::getline(configFile, agendaFilename); 
        } 
    }
    catch (...) {
    }

    openFile(agendaFilename);
}

void Agenda::reset() {
    setAgendaFilename("");
    _events.clear();
    _todos.clear();
}

const std::string & Agenda::getAgendaFilename() const {
    return _agendaFilename;
}

char * Agenda::getLine(char * str, size_t size, void * fd) {
    return fgets(str, size, (FILE*)fd);
}

std::vector<int> Agenda::computeEvents(const std::time_t & fromTime, const std::time_t & toTime) const {

    std::vector<int> eventIndices;

    for (unsigned i=0; i<_events.size(); i++) {
        const Event & e = _events.at(i);
        if (e._startTime <= toTime and e._endTime >= fromTime)
            eventIndices.push_back(i);
    }

    return eventIndices;
}

void Agenda::openFile(const std::string & filename) {

    if (not filename.empty()) {
        // clear previous data
        _events.clear();
        setAgendaFilename(filename);

        // parse file
        FILE * stream = fopen(_agendaFilename.c_str(), "r");
        if (stream == 0) return;
        icalparser * parser = icalparser_new();
        icalparser_set_gen_data(parser, stream);
        icalcomponent * component = icalparser_parse(parser, getLine);
        icalparser_free(parser);
        fclose(stream);

        // parse data 
        if (component != 0) {
            // events
            for (icalcomponent * c = icalcomponent_get_first_component(component, ICAL_VEVENT_COMPONENT);
                    c != 0;
                    c = icalcomponent_get_next_component(component, ICAL_VEVENT_COMPONENT)) {

                _events.emplace_back(c);
            }
            // todos
            for (icalcomponent * c = icalcomponent_get_first_component(component, ICAL_VTODO_COMPONENT);
                    c != 0;
                    c = icalcomponent_get_next_component(component, ICAL_VTODO_COMPONENT)) {

                _todos.emplace_back(c);
            }

            icalcomponent_free(component);
        }
    }
}

bool Agenda::canSaveFile() const {
    return not _agendaFilename.empty();
}

void Agenda::saveFile() {

    assert(canSaveFile());

    // create ical calendar
    icalcomponent * ptrIcalCalendar = icalcomponent_new(ICAL_VCALENDAR_COMPONENT);
    for (Event & event : _events) { 
        event.updateIcalFromData();
        icalcomponent * ptrIcalEvent = event.getPtrIcal();
        if (ptrIcalEvent and icalcomponent_is_valid(ptrIcalEvent)) {
            icalcomponent * ptrClone = icalcomponent_new_clone(ptrIcalEvent);
            if (ptrClone)
                icalcomponent_add_component(ptrIcalCalendar, ptrClone);
        }
    }
    for (Todo & todo : _todos) { 
        todo.updateIcalFromData();
        icalcomponent * ptrIcalEvent = todo.getPtrIcal();
        if (ptrIcalEvent and icalcomponent_is_valid(ptrIcalEvent)) {
            icalcomponent * ptrClone = icalcomponent_new_clone(ptrIcalEvent);
            if (ptrClone)
                icalcomponent_add_component(ptrIcalCalendar, ptrClone);
        }
    }

    // remove previous file
    { std::ofstream(_agendaFilename.c_str()); }

    // write calendar in the file
    icalset * ptrIcalset = icalset_new_file(_agendaFilename.c_str());
    icalset_add_component(ptrIcalset, ptrIcalCalendar);
    icalset_free(ptrIcalset);
}

void Agenda::saveAsFile(const std::string & filename) {
    setAgendaFilename(filename);
    saveFile();
}

void Agenda::setAgendaFilename(const std::string & filename) {
    _agendaFilename = filename;

    // write new agenda filename in config file
    std::ofstream configFile(_configPath);
    try {
        if (configFile) 
            configFile << _agendaFilename << std::endl;
    }
    catch (...) {
    }
}

