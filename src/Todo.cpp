// Copyright © 2014 Julien Dehos <dehos@lisic.univ-littoral.fr>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

#include "Todo.hpp"

#include <iostream>

Todo::Todo(const std::string & summary, const std::string & location, const std::string & description) :
    _ptrIcal(0), _summary(summary), _location(location), _description(description) {
        updateIcalFromData();
    }

Todo::Todo() : _ptrIcal(nullptr) {
}

Todo::Todo(icalcomponent * ptrIcal) {
    _ptrIcal = icalcomponent_new_clone(ptrIcal);
    updateDataFromIcal();
}

 
Todo::Todo (const Todo & event) : 
_summary(event._summary), _location(event._location), _description(event._description) {

    if (event._ptrIcal) {
        _ptrIcal = icalcomponent_new_clone(event._ptrIcal);
    }
    else {
        _ptrIcal = nullptr;
    }
}

icalcomponent * Todo::getPtrIcal() const {
    return _ptrIcal;
}

void Todo::updateIcalFromData() {

    // clean event
    if (_ptrIcal) {
        assert(icalcomponent_isa(_ptrIcal) == ICAL_VTODO_COMPONENT);
        removeProperty(ICAL_SUMMARY_PROPERTY);
        removeProperty(ICAL_LOCATION_PROPERTY);
        removeProperty(ICAL_DESCRIPTION_PROPERTY);
    }
    else {
        _ptrIcal = icalcomponent_new(ICAL_VTODO_COMPONENT);
        assert(_ptrIcal);
    }

    // add new properties
    if (not _summary.empty())
        icalcomponent_add_property(_ptrIcal, icalproperty_new_summary(_summary.c_str()));

    if (not _location.empty())
        icalcomponent_add_property(_ptrIcal, icalproperty_new_location(_location.c_str()));

    if (not _description.empty())
        icalcomponent_add_property(_ptrIcal, icalproperty_new_description(_description.c_str()));
}

void Todo::updateDataFromIcal() {
    assert(_ptrIcal);
    assert(icalcomponent_isa(_ptrIcal) == ICAL_VTODO_COMPONENT);

    for (icalproperty * ptrProperty = icalcomponent_get_first_property(_ptrIcal, ICAL_ANY_PROPERTY);
            ptrProperty != 0;
            ptrProperty = icalcomponent_get_next_property(_ptrIcal, ICAL_ANY_PROPERTY)) {

        icalproperty_kind propertyKind = icalproperty_isa(ptrProperty);

        switch (propertyKind) {
            case ICAL_SUMMARY_PROPERTY:     convertIcalToString(_summary, ptrProperty);         break;
            case ICAL_LOCATION_PROPERTY:    convertIcalToString(_location, ptrProperty);        break;
            case ICAL_DESCRIPTION_PROPERTY: convertIcalToString(_description, ptrProperty);     break;
            default: break;
        }
    }
}

void Todo::convertIcalToString(std::string & refString, icalproperty * ptrProperty) {
    refString = std::string(icalproperty_get_value_as_string(ptrProperty));
    // TODO check if libical has a function to remove escaped characters
    while (true) {
        std::string::size_type pos = refString.find("\\n");
        if (pos == std::string::npos) break;
        refString.replace(pos, 2, "\n");
    }
    while (true) {
        std::string::size_type pos = refString.find("\\t");
        if (pos == std::string::npos) break;
        refString.replace(pos, 2, "\t");
    }
    while (true) {
        std::string::size_type pos = refString.find("\\");
        if (pos == std::string::npos) break;
        refString.replace(pos, 1, "");
    }
}


void Todo::removeProperty(icalproperty_kind kind) {

    while (true) {

        icalproperty * ptrOldProperty = icalcomponent_get_first_property(_ptrIcal, kind);

        if (ptrOldProperty == nullptr) 
            break;
        else
            icalcomponent_remove_property(_ptrIcal, ptrOldProperty);
    }
}

